/* Физические величины и функции */

#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include <string>

class Physics
{
public:

    static std::vector<std::vector<double>> accumCoef;
    static double aW;
    static double aI;
    static double lambdaW;
    static double lambdaI;
    static double Q;
    static double TGrad;
    static double TYearDelta;
    static double TYearMiddle;
    static double H_0;
    static double rhoW;
    static double rhoI;
    static double gC;

    static double accumulation(double t);
    static void InitAccumCoef(std::string filepath);

    static double TSurface(double t, double Height);
    static double TFront(double L, double f);

    static double Surface(double t, double LPrev, double dt, double df);

    Physics();
    ~Physics();
};

#endif /* PHYSICS_H */
/* Класс моделирования методом ловли фронта */

#include "VTS.h"
#include "Config.h"
#include "Input.h"
#include "Physics.h"

#include <cmath>
#include <fstream>
#include <iostream>

using P = Physics;

VTS::VTS(InputString inputString) // Конструктор задачи для решения методом VTS
{

    xx = inputString.x;
    yy = inputString.y;
    tWrotenPrev = 0;

    // Размерность задачи
    n = std::round(Config::nodesPerMeter * (inputString.surface - inputString.bedrock)) + 1;

    // Координатная сетка для y
    y.resize(n, 0.0);
    dy.resize(n - 1, 0.0);
    mesh();

    // Временная сетка
    tCur = 0;
    dt = Config::timeStep;
    dtVTS = dt;

    // Поток и высота дна
    flux = inputString.flux;
    bedrock = inputString.bedrock;

    // Положение поверхности и фронта относительно дна
    LCur = inputString.surface - inputString.bedrock;

    double val = LCur;
    for (int i = 0; i < n; i++)
        if (fabs(LCur - inputString.iceThickness - y[i] * LCur) < val)
        {
            fIntCur = i;
            fDoubleCur = y[i] * LCur;
            val = fabs(LCur - inputString.iceThickness - y[i] * LCur);
        }

    // Определение количества и типов фаз
    if (fIntCur == 0)
        type = "I";
    else if (fIntCur == n - 1)
        type = "W";
    else
        type = "WI";

    // Подготовка матрицы системы и векторов
    TPrev.resize(n);
    TCur.resize(n);
    TCurIter.resize(n);
    M = Matrix3(n);

    // Параметры погрешности итераций и их количества
    iterationsLimiter = Config::iterationsLimiter;
    iterationsCounter = 0;
    iterationsAccuracy = Config::iterationsAccuracy;
    err = 0;

    // Заполнение вектора температуры начальными данными следующим образом:
    // фаза воды в т/д равновесии с учетом потока на дне
    // фаза льда интерполируется линейно по положению и температуре в точках фронта и поверхности

    TCur[n - 1] = P::TSurface(0, LCur + inputString.bedrock);

    if (type == "W")
    {
        for (int i = n - 2; i >= 0; i--)
        {
            TCur[i] = TCur[fIntCur] + inputString.flux * LCur / P::lambdaW * (1 - y[i]);
        }
    }

    if (type == "I")
    {
        TCur[0] = P::TFront(LCur, fDoubleCur);

        for (int i = 0; i < n; i++)
        {
            TCur[i] = y[i] * (TCur[n - 1] - TCur[0]) + TCur[0];
        }
    }

    if (type == "WI")
    {

        TCur[fIntCur] = P::TFront(LCur, fDoubleCur);

        for (int i = fIntCur - 1; i >= 0; i--)
        {
            TCur[i] = TCur[fIntCur] + inputString.flux * LCur / P::lambdaW * (y[fIntCur] - y[i]);
        }
        for (int i = fIntCur + 1; i < n; i++)
        {
            TCur[i] = (y[i] / (1 - y[i]) - y[i]) * (TCur[n - 1] - TCur[fIntCur]) + TCur[fIntCur];
        }
    }

    filename = "../Output/" +
               std::to_string(xx) + "," + std::to_string(yy) +
               "_VTS" +
               "_" + Config::scheme +
               "_" + Config::mesh;

    std::ofstream file(filename + "_surf.txt");
    file.clear();
    file.close();

    file.open(filename + "_T.txt");
    file.clear();
    file.close();

    print();

    printDescription();
}

void VTS::step() // Переход задачи на dt и решение методом VTS
{
    // ЗАПИСЬ ПЕРЕМЕННЫХ НА ПРЕДЫДУЩЕМ ВРЕМЕННОМ СЛОЕ //
    LPrev = LCur;                   //
                                    //
    fIntPrev = fIntCur;             //
    fDoublePrev = fDoubleCur;       //
                                    //
    tPrev = tCur;                   //
                                    //
    Maths::CopyVector(TCur, TPrev); //

    // СЛУЧАЙ ОДНОЙ ФАЗЫ: dt фиксировано, df = 0 //
    if (type != "WI")                           //
    {                                           //
        tCur = tPrev + dt;                      // Вычисление tCur и LCur
        LCur = P::Surface(tPrev, LPrev, dt, 0); // так как dt фиксировано
                                                //
        if (type == "W")                        // Вода
        {                                       //
            fIntCur = n - 1;                    //
            fDoubleCur = LCur;                  //
                                                //
            system();                           //
        }                                       //
        if (type == "I")                        // Лед
        {                                       //
            fIntCur = 0;                        //
            fDoubleCur = 0;                     //
                                                //
            system();                           //
        }                                       //
        Maths::Thomas(M, TCur);                 //
    }                                           //

    // СЛУЧАЙ ДВУХ ФАЗ: df=+-dy, итерационно ищем dt //
    else                       //
    {                          //
        iterationsCounter = 0; //
        initGuess();           // начальное приближение для dt и dy

        do                                                                      //
        {                                                                       //
            Maths::CopyVector(TCur, TCurIter);                                  //
                                                                                //
            calc_dtVTS();                                                       //
                                                                                //
            fIntCur = fIntPrev + Maths::sign(dtVTS);                            //
            fDoubleCur = y[fIntCur] * LCur;                                     //
                                                                                //
            tCur = tPrev + fabs(dtVTS);                                         //
                                                                                //
            LCur = P::Surface(tPrev, LPrev, dtVTS, dyVTS * Maths::sign(dtVTS)); //
            dL = LCur - LPrev;                                                  //
            system();                                                           //
                                                                                //
            Maths::Thomas(M, TCur);                                             //
                                                                                //
            err = Maths::Norm(TCur, TCurIter);                                  //
            iterationsCounter++;
            if (iterationsCounter >= iterationsLimiter)
                std::cout << "+" + '\n';                                             //
                                                                                     //
        } while (err > iterationsAccuracy && iterationsCounter < iterationsLimiter); //
    }                                                                                //

    findFront();

    print();

    /* write */
    /* save vars */
}

void VTS::system() // Процедура для заполнения системы в методе VTS
{
    // Очистка системы перед заполнением

    for (int i = 0; i < n; i++)
    {
        M.diag[i] = 0;
        M.low[i] = 0;
        M.up[i] = 0;

        TCur[i] = 0;
    }

    // Заполнение системы

    if (Config::scheme == "implicit")
    {

        if (type == "W")
        {
            M.diag[0] = 2 * P::aW * fabs(dtVTS) + dy[0] * dy[0] * LCur * LCur;
            M.up[0] = -2 * P::aW * fabs(dtVTS);
            TCur[0] = TPrev[0] * dy[0] * dy[0] * LCur * LCur + 2 * P::aW * fabs(dtVTS) * dy[0] * LCur * flux / P::lambdaW;

            M.diag[n - 1] = 1;
            TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

            for (int i = 1; i < n - 1; i++)
            {
                M.low[i] = -dy[i - 1] * dy[i] * LCur * LCur * D(i) +
                           LCur * y[i] * dL / 3.0 * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                           2 * P::aW * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.diag[i] = dy[i - 1] * dy[i] * LCur * LCur -
                            LCur * y[i] * dL / 3.0 * (dy[i] - dy[i - 1]) +
                            2 * P::aW * fabs(dtVTS);
                M.up[i] = dy[i - 1] * dy[i] * LCur * LCur * D(i) -
                          LCur * y[i] * dL / 3.0 * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                          2 * P::aW * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);

                TCur[i] = TPrev[i - 1] * (dy[i - 1] * dy[i] * LCur * LCur * D(i)) +
                          TPrev[i] * (dy[i - 1] * dy[i] * LCur * LCur) +
                          TPrev[i + 1] * (-dy[i - 1] * dy[i] * LCur * LCur * D(i));
            }
        }
        if (type == "I")
        {
            M.diag[0] = 2 * P::aI * fabs(dtVTS) + dy[0] * dy[0] * LCur * LCur;
            M.up[0] = -2 * P::aI * fabs(dtVTS);
            TCur[0] = TPrev[0] * dy[0] * dy[0] * LCur * LCur + 2 * P::aI * fabs(dtVTS) * dy[0] * LCur * flux / P::lambdaI;

            M.diag[n - 1] = 1;
            TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

            for (int i = 1; i < n - 1; i++)
            {
                M.low[i] = -dy[i - 1] * dy[i] * LCur * LCur * D(i) +
                           LCur * y[i] * dL / 3.0 * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                           2 * P::aI * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.diag[i] = dy[i - 1] * dy[i] * LCur * LCur -
                            LCur * y[i] * dL / 3.0 * (dy[i] - dy[i - 1]) +
                            2 * P::aI * fabs(dtVTS);
                M.up[i] = dy[i - 1] * dy[i] * LCur * LCur * D(i) -
                          LCur * y[i] * dL / 3.0 * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                          2 * P::aI * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);

                TCur[i] = TPrev[i - 1] * (dy[i - 1] * dy[i] * LCur * LCur * D(i)) +
                          TPrev[i] * (dy[i - 1] * dy[i] * LCur * LCur) +
                          TPrev[i + 1] * (-dy[i - 1] * dy[i] * LCur * LCur * D(i));
            }
        }
        if (type == "WI")
        {
            M.diag[0] = 2 * P::aW * fabs(dtVTS) + dy[0] * dy[0] * LCur * LCur;
            M.up[0] = -2 * P::aW * fabs(dtVTS);

            TCur[0] = TPrev[0] * dy[0] * dy[0] * LCur * LCur + 2 * P::aW * fabs(dtVTS) * dy[0] * LCur * flux / P::lambdaW;

            for (int i = 1; i < fIntCur; i++)
            {
                M.low[i] = -dy[i - 1] * dy[i] * LCur * LCur * D(i) +
                           LCur / 3.0 * (y[i] * dL) * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                           2 * P::aW * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.diag[i] = dy[i - 1] * dy[i] * LCur * LCur -
                            LCur / 3.0 * (y[i] * dL) * (dy[i] - dy[i - 1]) +
                            2 * P::aW * fabs(dtVTS);
                M.up[i] = dy[i - 1] * dy[i] * LCur * LCur * D(i) -
                          LCur / 3.0 * (y[i] * dL) * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                          2 * P::aW * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);

                TCur[i] = TPrev[i - 1] * (dy[i - 1] * dy[i] * LCur * LCur * D(i)) +
                          TPrev[i] * (dy[i - 1] * dy[i] * LCur * LCur) +
                          TPrev[i + 1] * (-dy[i - 1] * dy[i] * LCur * LCur * D(i));
            }

            M.diag[fIntCur] = 1;
            TCur[fIntCur] = P::TFront(LCur, fDoubleCur);

            for (int i = fIntCur + 1; i < n - 1; i++)
            {
                M.low[i] = -dy[i - 1] * dy[i] * LCur * LCur * D(i) +
                           LCur / 3.0 * (y[i] * dL + Config::IceMovementMode * (1 - P::aI / P::aW) * (dyVTS / Maths::sign(dtVTS) * LCur + dL * y[fIntCur])) * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                           2 * P::aI * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.diag[i] = dy[i - 1] * dy[i] * LCur * LCur -
                            LCur / 3.0 * (y[i] * dL + Config::IceMovementMode * (1 - P::aI / P::aW) * (dyVTS / Maths::sign(dtVTS) * LCur + dL * y[fIntCur])) * (dy[i] - dy[i - 1]) +
                            2 * P::aI * fabs(dtVTS);
                M.up[i] = dy[i - 1] * dy[i] * LCur * LCur * D(i) -
                          LCur / 3.0 * (y[i] * dL + Config::IceMovementMode * (1 - P::aI / P::aW) * (dyVTS / Maths::sign(dtVTS) * LCur + dL * y[fIntCur])) * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                          2 * P::aI * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);

                TCur[i] = TPrev[i - 1] * (dy[i - 1] * dy[i] * LCur * LCur * D(i)) +
                          TPrev[i] * (dy[i - 1] * dy[i] * LCur * LCur) +
                          TPrev[i + 1] * (-dy[i - 1] * dy[i] * LCur * LCur * D(i));
            }

            M.diag[n - 1] = 1;
            TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);
        }
    }
    if (Config::scheme == "CN")
    {
        if (type == "W")
        {
            M.diag[0] = 2 * P::aW * fabs(dtVTS) + dy[0] * dy[0] * LCur * LCur;
            M.up[0] = -2 * P::aW * fabs(dtVTS);
            TCur[0] = TPrev[0] * dy[0] * dy[0] * LCur * LCur + 2 * P::aW * fabs(dtVTS) * dy[0] * LCur * flux / P::lambdaW;

            M.diag[n - 1] = 1;
            TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

            for (int i = 1; i < n - 1; i++)
            {
                double value = -dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) +
                               (LCur + LPrev) / 6.0 * y[i] * dL * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                               2 * P::aW * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.low[i] = value;
                TCur[i] += TPrev[i - 1] * (-value);

                value = -(LCur + LPrev) / 6.0 * y[i] * dL * (dy[i] - dy[i - 1]) +
                        2 * P::aW * fabs(dtVTS);
                M.diag[i] = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) +
                            value;
                TCur[i] += TPrev[i] * (dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) -
                                       value);

                value = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) -
                        (LCur + LPrev) / 6.0 * y[i] * dL * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                        2 * P::aW * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);
                M.up[i] = value;
                TCur[i] += TPrev[i + 1] * (-value);
            }
        }
        if (type == "I")
        {
            M.diag[0] = 2 * P::aI * fabs(dtVTS) + dy[0] * dy[0] * LCur * LCur;
            M.up[0] = -2 * P::aI * fabs(dtVTS);
            TCur[0] = TPrev[0] * dy[0] * dy[0] * LCur * LCur + 2 * P::aI * fabs(dtVTS) * dy[0] * LCur * flux / P::lambdaI;

            M.diag[n - 1] = 1;
            TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

            for (int i = 1; i < n - 1; i++)
            {
                double value = -dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) +
                               (LCur + LPrev) / 6.0 * y[i] * dL * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                               2 * P::aI * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.low[i] = value;
                TCur[i] += TPrev[i - 1] * (-value);

                value = -(LCur + LPrev) / 6.0 * y[i] * dL * (dy[i] - dy[i - 1]) +
                        2 * P::aI * fabs(dtVTS);
                M.diag[i] = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) +
                            value;
                TCur[i] += TPrev[i] * (dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) -
                                       value);

                value = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) -
                        (LCur + LPrev) / 6.0 * y[i] * dL * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                        2 * P::aI * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);
                M.up[i] = value;
                TCur[i] += TPrev[i + 1] * (-value);
            }
        }
        if (type == "WI")
        {
            M.diag[0] = 2 * P::aW * fabs(dtVTS) + dy[0] * dy[0] * LCur * LCur;
            M.up[0] = -2 * P::aW * fabs(dtVTS);

            TCur[0] = TPrev[0] * dy[0] * dy[0] * LCur * LCur + 2 * P::aW * fabs(dtVTS) * dy[0] * LCur * flux / P::lambdaW;

            for (int i = 1; i < fIntCur; i++)
            {
                double value = -dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) +
                               (LCur + LPrev) / 6.0 * (y[i] * dL) * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                               2 * P::aW * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.low[i] = value;
                TCur[i] += TPrev[i - 1] * (-value);

                value = -(LCur + LPrev) / 6.0 * (y[i] * dL) * (dy[i] - dy[i - 1]) +
                        2 * P::aW * fabs(dtVTS);
                M.diag[i] = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) +
                            value;
                TCur[i] += TPrev[i] * (dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) -
                                       value);

                value = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) -
                        (LCur + LPrev) / 6.0 * (y[i] * dL) * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                        2 * P::aW * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);
                M.up[i] = value;
                TCur[i] += TPrev[i + 1] * (-value);
            }

            M.diag[fIntCur] = 1;
            TCur[fIntCur] = P::TFront(LCur, fDoubleCur);

            for (int i = fIntCur + 1; i < n - 1; i++)
            {
                double value = -dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) +
                               (LCur + LPrev) / 6.0 * (y[i] * dL + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * (dyVTS / Maths::sign(dtVTS) * (LCur + LPrev) / 2.0 + dL * (y[fIntCur] + y[fIntPrev]) / 2.0)) * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                               2 * P::aI * fabs(dtVTS) * dy[i] / (dy[i - 1] + dy[i]);
                M.low[i] = value;
                TCur[i] += TPrev[i - 1] * (-value);

                value = -(LCur + LPrev) / 6.0 * (y[i] * dL + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * (dyVTS / Maths::sign(dtVTS) * (LCur + LPrev) / 2.0 + dL * (y[fIntCur] + y[fIntPrev]) / 2.0)) * (dy[i] - dy[i - 1]) +
                        2 * P::aI * fabs(dtVTS);
                M.diag[i] = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) +
                            value;
                TCur[i] += TPrev[i] * (dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) -
                                       value);

                value = dy[i - 1] * dy[i] * (LCur * LCur + LPrev * LPrev) * D(i) -
                        (LCur + LPrev) / 6.0 * (y[i] * dL + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * (dyVTS / Maths::sign(dtVTS) * (LCur + LPrev) / 2.0 + dL * (y[fIntCur] + y[fIntPrev]) / 2.0)) * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                        2 * P::aI * fabs(dtVTS) * dy[i - 1] / (dy[i - 1] + dy[i]);
                M.up[i] = value;
                TCur[i] += TPrev[i + 1] * (-value);
            }

            M.diag[n - 1] = 1;
            TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);
        }
    }
}

void VTS::calc_dtVTS() // Вычисление dtVTS
{
    if (fIntCur == 0) // Если фронт смещается в узел 0
    {
        dtVTSup = dyVTS * dyVTS * LCur * LCur * P::Q * (P::rhoW + P::rhoI) / 2;
        dtVTSlow1 = dyVTS * LCur * flux;
        dtVTSlow2 = P::lambdaI * (2 * P::aI * fabs(dtVTS) * (TCurIter[1] - TCurIter[0]) - dyVTS * dyVTS * LCur * LCur * (TCurIter[0] - TPrev[0])) /
                    (2 * P::aI * fabs(dtVTS) - Config::IceMovementMode * dyVTS * LCur * (1 - P::rhoI / P::rhoW) * dyVTS * LCur / Maths::sign(dtVTS));
    }
    if (fIntCur == n - 1) // Если фронт смещается в узел n-1
    {
        dtVTSup = dyVTS * LCur * P::Q * (P::rhoW + P::rhoI) / 2 * (dyVTS * LCur + (LCur - LPrev) / Maths::sign(dtVTS));
        dtVTSlow1 = -P::lambdaW * (2 * P::aW * fabs(dtVTS) * (TCurIter[n - 1] - TCurIter[n - 2]) + dyVTS * dyVTS * LCur * LCur * (TCurIter[n - 1] - TPrev[n - 1])) /
                    (2 * P::aW * fabs(dtVTS) + dyVTS * LCur * (LCur - LPrev));
        dtVTSlow2 = 0;
    }
    if (fIntCur != 0 && fIntCur != n - 1) // Если фронт смещается в неграничный узел
    {
        dtVTSup = dyVTS * LCur * P::Q * (P::rhoI + P::rhoW) / 2 * (dyVTS * LCur + (LCur - LPrev) / Maths::sign(dtVTS) * y[fIntCur]);
        dtVTSlow1 = -P::lambdaW * (2 * P::aW * fabs(dtVTS) * (TCurIter[fIntCur] - TCurIter[fIntCur - 1]) + dyVTS * dyVTS * LCur * LCur * (TCurIter[fIntCur] - TPrev[fIntCur])) /
                    (2 * P::aW * fabs(dtVTS) + y[fIntCur] * dyVTS * LCur * (LCur - LPrev));
        dtVTSlow2 = P::lambdaI * (2 * P::aI * fabs(dtVTS) * (TCurIter[fIntCur + 1] - TCurIter[fIntCur]) - dyVTS * dyVTS * LCur * LCur * (TCurIter[fIntCur] - TPrev[fIntCur])) /
                    (2 * P::aI * fabs(dtVTS) - y[fIntCur] * dyVTS * LCur * (LCur - LPrev) - Config::IceMovementMode * dyVTS * LCur * (1 - P::rhoI / P::rhoW) * (dyVTS * LCur / Maths::sign(dtVTS) + (LCur - LPrev) * y[fIntCur]));
    }

    dtVTS = dtVTSup / (dtVTSlow1 + dtVTSlow2);
}

void VTS::findFront() // Поиск положения фронта в методе VTS

{                                                                                                                     //
    bool flag = 0;                                                                                                    //
    for (int i = 0; i < n - 1; i++)                                                                                   //
        if ((TCur[i] - P::TFront(LCur, y[i] * LCur)) * (TCur[i + 1] - P::TFront(LCur, y[i + 1] * LCur)) <= 0)         //
        {                                                                                                             //
            if (fabs(TCur[i] - P::TFront(LCur, y[i] * LCur)) <= fabs(TCur[i + 1] - P::TFront(LCur, y[i + 1] * LCur))) //
            {                                                                                                         //
                fIntCur = i;                                                                                          //
                fDoubleCur = y[i] * LCur;                                                                             //
                TCur[i] = P::TFront(LCur, fDoubleCur);                                                                //
            }                                                                                                         //
            else                                                                                                      //
            {                                                                                                         //
                fIntCur = i + 1;                                                                                      //
                fDoubleCur = y[i + 1] * LCur;                                                                         //
                TCur[i + 1] = P::TFront(LCur, fDoubleCur);                                                            //
            }                                                                                                         //
            flag = 1;                                                                                                 //
            type = "WI";                                                                                              //
            break;                                                                                                    //
        }                                                                                                             //
    if (flag == 0)                                                                                                    //
    {                                                                                                                 //
        if (TCur.back() < 0)
        {
            type = "I";
            fDoubleCur = 0;
            fIntCur = 0;
        }
        else
        {
            type = "W";
            fDoubleCur = LCur;
            fIntCur = n - 1;
        }
    }
}

VTS::~VTS()
{
}

void VTS::print()
{
    std::ofstream file(filename + "_surf.txt", std::ios::app);
    file << tCur / 31536000 << '\t'
         << fDoubleCur << '\t'
         << LCur << '\n';
    file.close();

    file.open(filename + "_T.txt");
    file.clear();
    for (int i = 0; i < n; i++)
        file << y[i] * LCur << '\t'
             << TCur[i] << '\n';
    file.close();
}

void VTS::mesh()
{
    if (Config::mesh == "uni")
        for (int i = 0; i < n; i++)
            y[i] = 1.0 / (n - 1) * i;
    if (Config::mesh == "nonuni")
    {
        std::ofstream file("../Sigmoid.txt");
        for (int i = 0; i < n; i++)
        {
            y[i] = sigmoidF(1.0 / (n - 1) * i);
            file << 1.0 / (n - 1) * i << '\t' << y[i] << '\n';
        }
        file.close();
    }
    for (int i = 0; i < n - 1; i++)
        dy[i] = y[i + 1] - y[i];
}

double VTS::sigmoidF(double x)
{
    double res = 0.0;
    res = 0.6 * (x - 0.5) / (0.1 + fabs(x - 0.5)) + 0.5;
    return res;
}

double VTS::D(int i)
{
    double res = 0.0;
    res = (dy[i] - dy[i - 1]) / (dy[i] + dy[i - 1]) / 3.0;
    return res;
}

void VTS::initGuess()
{
    double value = -P::lambdaW * (TPrev[fIntPrev] - TPrev[fIntPrev - 1]) / dy[fIntPrev - 1] +
                   P::lambdaI * (TPrev[fIntPrev + 1] - TPrev[fIntPrev]) / dy[fIntPrev];
    if (value < 0)
    {
        dtVTS = -dt;
        dyVTS = dy[fIntPrev - 1];
    }
    else
    {
        dtVTS = dt;
        dyVTS = dy[fIntPrev];
    }
}

void VTS::printDescription()
{
    std::ofstream file(filename + ".txt");
    file.clear();

    file << "x = " << xx << '\n'
         << "y = " << yy << '\n'
         << "L = " << LCur << " ,m" << '\n'
         << "N = " << n << '\n'
         << "dt = " << dt / 3600 << " ,h" << '\n'
         << "Basal flux = " << flux << " ,W" << '\n'
         << "Method: VTS" << '\n'
         << "Scheme: " << Config::scheme << '\n'
         << "Mesh: " << Config::mesh + "form";

    file.close();
}

/* Класс моделирования процесса */

#include "FFM.h"
#include "Config.h"
#include "Input.h"
#include "Physics.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>

//
using P = Physics;

FFM::FFM(InputString inputString) // Конструктор задачи для решения методом FFM
{
    //
    xx = inputString.x;
    yy = inputString.y;
    filename = "../Output/" +
               std::to_string(xx) + "," + std::to_string(yy) +
               "_FFM" +
               +"_" + Config::scheme +
               "_" + Config::mesh;

    std::ofstream file(filename + "_surf.txt");
    file.clear();
    file.close();

    file.open(filename + "_T.txt");
    file.clear();
    file.close();

    // Размерность задачи
    n = std::round(Config::nodesPerMeter * (inputString.surface - inputString.bedrock)) + 1;

    // Координатная сетка для y
    y.resize(n, 0.0);
    dy.resize(n - 1, 0.0);
    mesh();

    // Временная сетка
    tCur = 0;
    tPrev = 0;
    dt = Config::timeStep;
    tWrotenPrev = 0;

    // Поток и высота дна
    flux = inputString.flux;

    // Положение поверхности и фронта относительно дна
    LCur = inputString.surface - inputString.bedrock;
    LPrev = 0.0;
    dL = 0.0;

    fCur = LCur - inputString.iceThickness;
    fPrev = 0.0;
    df = 0;
    df1 = 0;
    df2 = 0;
    bedrock = inputString.bedrock;

    // Определение количества и типов фаз
    if (fCur <= y[1] * LCur)
    {
        fCur = 0;
        type = "I";
    }
    else if (fCur >= y[n - 2] * LCur)
    {
        fCur = LCur;
        type = "W";
    }
    else
        type = "WI";

    // Подготовка матриц и векторов системы
    TPrevI.resize(n);
    TCurI.resize(n);
    TCurIterI.resize(n);
    MI = Matrix3(n);

    TPrevW.resize(n);
    TCurW.resize(n);
    TCurIterW.resize(n);
    MW = Matrix3(n);

    // Параметры погрешности итераций и их количества
    iterationsLimiter = Config::iterationsLimiter;
    iterationsCounter = 0;
    iterationsAccuracy = Config::iterationsAccuracy;
    errW = 0;
    errI = 0;

    // Заполнение вектора температуры начальными данными следующим образом:
    // фаза воды в т/д равновесии с учетом потока на дне
    // фаза льда интерполируется линейно по положению и температуре в точках фронта и поверхности

    // заполнение w
    if (type == "I")
    {
        TCurW.assign(n, P::TFront(LCur, 0.0));
    }
    else
    {
        TCurW[n - 1] = P::TFront(LCur, fCur);
        for (int i = n - 2; i >= 0; i--)
        {
            TCurW[i] = TCurW[n - 1] + inputString.flux * fCur / P::lambdaW * (1 - y[i]);
        }
    }

    // заполнение i
    if (type == "W")
    {
        TCurI.assign(n, P::TSurface(tCur, LCur + bedrock));
    }
    else
    {
        TCurI[0] = TCurW[n - 1];
        TCurI[n - 1] = P::TSurface(0, LCur + inputString.bedrock);

        for (int i = 0; i < n; i++)
        {
            TCurI[i] = (TCurI[n - 1] - TCurI[0]) * y[i] + TCurI[0];
        }
    }


    print();

    printDescription();
}

void FFM::step() // Переход задачи на dt и решение методом FFM
{
    // ЗАПИСЬ ПЕРЕМЕННЫХ НА ПРЕДЫДУЩЕМ ВРЕМЕННОМ СЛОЕ //
    LPrev = LCur;                     //
    fPrev = fCur;         //
                                      //
    tPrev = tCur;                     //
    tCur = tPrev + dt;                //
                                      //
    Maths::CopyVector(TCurI, TPrevI); // Запись тепературы на предыдущем временном слое
    Maths::CopyVector(TCurW, TPrevW); //

    df = 0;                // Начальное приближение df
    iterationsCounter = 0; //

    do                                       //
    {                                        //
        Maths::CopyVector(TCurI, TCurIterI); // Копирование итерационной тепературы льда
        Maths::CopyVector(TCurW, TCurIterW); // Копирование итерационной тепературы воды
                                             //
        calc_df();                           // Оценка df
        fCur = fPrev + df;       //
        checkFront();

        //
        LCur = P::Surface(tPrev, LPrev, dt, df); // Оценки LCur и fCur
        dL = LCur - LPrev;
        //
        system();                             // Система
                                              //
        Maths::Thomas(MW, TCurW);             // Решение системы
        Maths::Thomas(MI, TCurI);             //
                                              //
        errW = Maths::Norm(TCurW, TCurIterW); // Погрешности итераций
        errI = Maths::Norm(TCurI, TCurIterI); //
                                              //
        iterationsCounter++;                  //
    } while ((errW > Config::iterationsAccuracy && errI > Config::iterationsAccuracy) && iterationsCounter < iterationsLimiter);

    print();
};

void FFM::system()
{
    // Очистка системы перед заполнением

    for (int i = 0; i < n; i++)
    {
        MW.diag[i] = 0;
        MW.low[i] = 0;
        MW.up[i] = 0;

        MI.diag[i] = 0;
        MI.low[i] = 0;
        MI.up[i] = 0;

        TCurW[i] = 0;
        TCurI[i] = 0;
    }

    // Заполнение системы

    if (Config::scheme == "implicit")
    {

        // заполняем W

        if (type == "I")
        {
            MW.diag.assign(n, 1.0);
            TCurW.assign(n, P::TFront(LCur, 0.0));
        }
        else
        {
            MW.diag[0] = 2 * P::aW * dt + dy[0] * dy[0] * fCur * fCur;
            MW.up[0] = -2 * P::aW * dt;
            TCurW[0] = TPrevW[0] * dy[0] * dy[0] * fCur * fCur + 2 * P::aW * dt * dy[0] * fCur * flux / P::lambdaW;

            for (int i = 1; i < n - 1; i++)
            {
                MW.low[i] = -dy[i - 1] * dy[i] * fCur * fCur * D(i) +
                            fCur / 3.0 * y[i] * df * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                            2.0 * P::aW * dt * dy[i] / (dy[i - 1] + dy[i]);
                MW.diag[i] = dy[i - 1] * dy[i] * fCur * fCur -
                             fCur /3.0 * y[i] * df * (dy[i] - dy[i - 1]) +
                             2.0 * P::aW * dt;
                MW.up[i] = dy[i - 1] * dy[i] * fCur * fCur * D(i) -
                           fCur / 3.0 * y[i] * df * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                           2.0 * P::aW * dt * dy[i - 1] / (dy[i - 1] + dy[i]);

                TCurW[i] = TPrevW[i - 1] * (dy[i - 1] * dy[i] * fCur * fCur * D(i)) +
                           TPrevW[i] * (dy[i - 1] * dy[i] * fCur * fCur) +
                           TPrevW[i + 1] * (-dy[i - 1] * dy[i] * fCur * fCur * D(i));
            }
            MW.diag[n - 1] = 1;
            TCurW[n - 1] = P::TFront(LCur, fCur);
        }

        // заполняем I

        if (type == "W")
        {
            MI.diag.assign(n, 1.0);
            TCurI.assign(n, P::TSurface(tCur, LCur + bedrock));
        }
        else
        {
            MI.diag[0] = 1;
            TCurI[0] = P::TFront(LCur, fCur);

            for (int i = 1; i < n - 1; i++)
            {
                MI.low[i] = -dy[i - 1] * dy[i] * (LCur - fCur) * (LCur - fCur) * D(i) +
                            (LCur - fCur) / 3.0 * ((1 - y[i]) * df + y[i] * dL + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * df) * (dy[i] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                            2.0 * P::aI * dt * dy[i] / (dy[i - 1] + dy[i]);
                MI.diag[i] = dy[i - 1] * dy[i] * (LCur - fCur) * (LCur - fCur) -
                             (LCur - fCur) / 3.0 * ((1 - y[i]) * df + y[i] * dL + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * df) * (dy[i] - dy[i - 1]) +
                             2.0 * P::aI * dt;
                MI.up[i] = dy[i - 1] * dy[i] * (LCur - fCur) * (LCur - fCur) * D(i) -
                           (LCur - fCur) / 3.0 * ((1 - y[i]) * df + y[i] * dL + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * df) * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                           2.0 * P::aI * dt * dy[i - 1] / (dy[i - 1] + dy[i]);

                TCurI[i] = TPrevI[i - 1] * (dy[i - 1] * dy[i] * (LCur - fCur) * (LCur - fCur) * D(i)) +
                           TPrevI[i] * (dy[i - 1] * dy[i] * (LCur - fCur) * (LCur - fCur)) +
                           TPrevI[i + 1] * (-dy[i - 1] * dy[i] * (LCur - fCur) * (LCur - fCur) * D(i));
            }

            MI.diag[n - 1] = 1;
            TCurI[n - 1] = P::TSurface(tCur, bedrock + LCur);
        }
    }
    if (Config::scheme == "CN")
    {
        // заполняем W

        if (type == "I")
        {
            MW.diag.assign(n, 1.0);
            TCurW.assign(n, P::TFront(LCur, 0.0));
        }
        else
        {
            MW.diag[0] = 2 * P::aW * dt + dy[0] * dy[0] * fCur * fCur;
            MW.up[0] = -2 * P::aW * dt;
            TCurW[0] = TPrevW[0] * dy[0] * dy[0] * fCur * fCur + 2 * P::aW * dt * dy[0] * fCur * flux / P::lambdaW;

            for (int i = 1; i < n - 1; i++)
            {
                double value = -dy[i - 1] * dy[i] * (fCur * fCur + fPrev * fPrev) * D(i) +
                               (fCur + fPrev) / 6.0 * y[i] * df * (dy[i] + (dy[i - 1] * dy[i]) / (dy[i - 1] + dy[i])) -
                               2.0 * P::aW * dt * dy[i] / (dy[i - 1] + dy[i]);
                MW.low[i] = value;
                TCurW[i] += TPrevW[i - 1] * (-value);

                value = -(fCur + fPrev) / 6.0 * y[i] * df * (dy[i] - dy[i - 1]) +
                        2 * P::aW * dt;
                MW.diag[i] = dy[i - 1] * dy[i] * (fCur * fCur + fPrev * fPrev) + value;
                TCurW[i] += TPrevW[i] * (dy[i - 1] * dy[i] * (fCur * fCur + fPrev * fPrev) - value);

                value = dy[i - 1] * dy[i] * (fCur * fCur + fPrev * fPrev) * D(i) -
                        (fCur + fPrev) / 6.0 * y[i] * df * (dy[i - 1] + (dy[i - 1] * dy[i]) / (dy[i - 1] + dy[i])) -
                        2.0 * P::aW * dt * dy[i - 1] / (dy[i - 1] + dy[i]);
                MW.up[i] = value;
                TCurW[i] += TPrevW[i + 1] * (-value);
            }
            MW.diag[n - 1] = 1;
            TCurW[n - 1] = P::TFront(LCur, fCur);
        }

        // заполняем I

        if (type == "W")
        {
            MI.diag.assign(n, 1.0);
            TCurI.assign(n, P::TSurface(tCur, LCur + bedrock));
        }
        else
        {
            MI.diag[0] = 1;
            TCurI[0] = P::TFront(LCur, fCur);

            for (int i = 1; i < n - 1; i++)
            {
                double value = -dy[i - 1] * dy[i] * ((LCur - fCur) * (LCur - fCur) + (LPrev - fPrev) * (LPrev - fPrev)) * D(i) +
                               ((LCur - fCur) + (LPrev - fPrev)) / 6.0 * ((1 - y[i]) * df + y[i] * (LCur - LPrev) + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * df) * (dy[i] + (dy[i - 1] * dy[i]) / (dy[i - 1] + dy[i])) -
                               2.0 * P::aI * dt * dy[i] / (dy[i - 1] + dy[i]);
                MI.low[i] = value;
                TCurI[i] += TPrevI[i - 1] * (-value);

                value = -((LCur - fCur) + (LPrev - fPrev)) / 6.0 * ((1 - y[i]) * df + y[i] * (LCur - LPrev) + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * df) * (dy[i] - dy[i - 1]) +
                        2 * P::aI * dt;
                MI.diag[i] = dy[i - 1] * dy[i] * ((LCur - fCur) * (LCur - fCur) + (LPrev - fPrev) * (LPrev - fPrev)) + value;
                TCurI[i] += TPrevI[i] * (dy[i - 1] * dy[i] * ((LCur - fCur) * (LCur - fCur) + (LPrev - fPrev) * (LPrev - fPrev)) - value);

                value = dy[i - 1] * dy[i] * ((LCur - fCur) * (LCur - fCur) + (LPrev - fPrev) * (LPrev - fPrev)) * D(i) -
                        ((LCur - fCur) + (LPrev - fPrev)) / 6.0 * ((1 - y[i]) * df + y[i] * (LCur - LPrev) + Config::IceMovementMode * (1 - P::rhoI / P::rhoW) * df) * (dy[i - 1] + dy[i - 1] * dy[i] / (dy[i - 1] + dy[i])) -
                        2.0 * P::aI * dt * dy[i - 1] / (dy[i - 1] + dy[i]);
                MI.up[i] = value;
                TCurI[i] += TPrevI[i + 1] * (-value);
            }

            MI.diag[n - 1] = 1;
            TCurI[n - 1] = P::TSurface(tCur, bedrock + LCur);
        }
    }
}

void FFM::calc_df() // Вычиление df

{
    if (type == "WI")
    {
        df1 = -P::lambdaW / fCur * (2 * P::aW * dt * (TCurIterW[n - 1] - TCurIterW[n - 2]) + dy[0] * dy[0] * fCur * fCur * (TCurIterW[n - 1] - TPrevW[n - 1])) / //
              (2 * P::aW * dt + dy[0] * fCur * df);                                                                                                                          //
    }
    else
    {
        df1 = flux * dy[0];
    }
    df2 = P::lambdaI / (LCur - fCur) * (2 * P::aI * dt * (TCurIterI[1] - TCurIterI[0]) - dy[0] * dy[0] * (LCur - fCur) * (LCur - fCur) * (TCurIterI[0] - TPrevI[0])) / //
          (2 * P::aI * dt - dy[0] * (LCur - fCur) * df - Config::IceMovementMode * dy[0] * (LCur - fCur) * (1 - P::rhoI / P::rhoW) * df);                                    //
                                                                                                                                                                                         //
    df = dt * (df1 + df2) /                                                                                                                                                              //
         (dy[0] * P::Q * (P::rhoI + P::rhoW) / 2);                                                                                                                                       //                                                                                                                                                                                 //
}

void FFM::mesh()
{
    if (Config::mesh == "uni")
        for (int i = 0; i < n; i++)
            y[i] =
                1.0 / (n - 1) * i;
    if (Config::mesh == "nonuni")
        for (int i = 0; i < n; i++)
            y[i] = sigmoidF(1.0 / (n - 1) * i);
    for (int i = 0; i < n - 1; i++)
        dy[i] = y[i + 1] - y[i];
}

double FFM::sigmoidF(double x)
{
    double res = 0.0;
    res = 0.6 * (x - 0.5) / (0.1 + fabs(x - 0.5)) + 0.5;
    return res;
}

double FFM::D(int i)
{
    double res = 0.0;
    res = (dy[i] - dy[i - 1]) / (dy[i] + dy[i - 1]) / 3.0;
    return res;
}

FFM::~FFM()
{
}

void FFM::print()
{
    /////////////////////////////////////////////
    std::ofstream file(filename + "_surf.txt", std::ios::app);

    std::cout.precision(6);
    file << tCur / 31536000 << '\t'
         << std::fixed << std::setprecision(6) << fCur << '\t'
         << std::fixed << std::setprecision(6) << LCur << '\n';

    file.close();
    /////////////////////////////////////////////
    file.open(filename + "_T.txt");
    file.clear();

    if (type == "I")
        for (int i = 0; i < n; i++)
            file << y[i] * LCur << '\t'
                 << TCurI[i] << '\n';

    else if (type == "W")
        for (int i = 0; i < n; i++)
            file << y[i] * LCur << '\t'
                 << TCurW[i] << '\n';

    else
    {
        for (int i = 0; i < n; i++)
            file << y[i] * fCur << '\t'
                 << TCurW[i] << '\n';
        for (int i = 0; i < n; i++)
            file << fCur + y[i] * (LCur - fCur) << '\t'
                 << TCurI[i] << '\n';
    }
    file.close();
}

void FFM::printDescription()
{
    std::ofstream file(filename + ".txt");
    file.clear();

    file << "x = " << xx << '\n'
         << "y = " << yy << '\n'
         << "L = " << LCur << " ,m" << '\n'
         << "N = " << n << '\n'
         << "dt = " << dt / 3600 << " ,h" << '\n'
         << "Basal flux = " << flux << " ,W" << '\n'
         << "Method: FFM" << '\n'
         << "Scheme: " << Config::scheme << '\n'
         << "Mesh: " << Config::mesh + "form";

    file.close();
}

void FFM::checkFront()
{
    if (fCur <= 0)
    {
        fCur = 0;
        type = "I";
        df = fCur - fPrev;
    }
    else if (fCur >= LCur)
    {
        fCur = LCur;
        type = "W";
        df = fCur - fPrev;
    }
    else
    {
        type = "WI";
    }
}
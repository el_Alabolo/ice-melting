/* Математические объекты и работа с ними */

#include "Maths.h"

#include <cmath>
#include <vector>

#include <iostream>

void Maths::Thomas(Matrix3 M, std::vector<double> &b) // Решает систему методом прогонки и записывает результат в b
{
    // Прямой проход

    int n = b.size();

    b[0] /= M.diag[0]; // Первая строка
    M.up[0] /= M.diag[0];
    M.diag[0] = 1;

    for (int i = 1; i < n - 1; i++) // Внутренние строки
    {
        b[i] -= b[i - 1] * M.low[i];
        M.diag[i] -= M.up[i - 1] * M.low[i];
        M.low[i] = 0;

        b[i] /= M.diag[i];
        M.up[i] /= M.diag[i];
        M.diag[i] = 1;
    }

    b[n - 1] -= b[n - 2] * M.low[n - 1];
    M.diag[n - 1] -= M.up[n - 2] * M.low[n - 1];
    M.low[n - 1] = 0;

    b[n - 1] /= M.diag[n - 1];
    M.diag[n - 1] = 1;

    // Обратный проход

    for (int i = n - 2; i > -1; i--) // Внутренние и первая строки
    {
        b[i] -= b[i+1] * M.up[i];
    }
}

double Maths::Norm(std::vector<double> v1, std::vector<double> v2)
{
    double res = 0;
    double normForElem = 0;

    for (int i = 0; i < v1.size(); i++)
    {
        normForElem = fabs(v1[i] - v2[i]);
        if (normForElem > res)
            res = normForElem;
    }

    return res;
}

void Maths::CopyVector(std::vector<double> v1, std::vector<double> &v2)
{
    int n = v1.size();
    for (int i = 0; i < n; i++)
    {
        v2[i] = v1[i];
    }
}

int Maths::sign(double x)
{
    if (x >= 0)
        return 1;
    else
        return -1;
}

Matrix3::Matrix3(int n)
{
    low.assign(n, 0.0);
    diag.assign(n, 0.0);
    up.assign(n, 0.0);
}

Matrix3::Matrix3()
{
}
Matrix3::~Matrix3()
{
}
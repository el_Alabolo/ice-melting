/* Физические величины и функции */

#include "Physics.h"
#include "Config.h"

#include <cmath>
#include <string>
#include <fstream>

double Physics::aW = 0.132e-6;
double Physics::aI = 1.08e-6;
double Physics::lambdaW = 0.57;
double Physics::lambdaI = 2.3;
double Physics::Q = 3.3e5;
double Physics::rhoW = 1e3;
double Physics::rhoI = 0.917e3;
double Physics::TGrad = -6.06e-3;
double Physics::TYearDelta = 16.0;
double Physics::TYearMiddle = -4.3;
double Physics::H_0 = 267;
double Physics::gC = 72.814e-8;

std::vector<std::vector<double>> Physics::accumCoef = std::vector<std::vector<double>>(3);

Physics::Physics()
{
}
Physics::~Physics()
{
}

double Physics::TSurface(double t, double Height) // height=L+bedrock, аргументы - безразм. величины
{
    double res;

    if (Config::TSurfaceMode == 1)
    {
        res = TYearMiddle + TYearDelta / 2 * sin(2 * M_PI * t / 31536000) + TGrad * (Height - H_0);
    }
    else
        res = Config::TSurface;

    if (res <= 0)
        return res;
    else
        return -0.1;
}

double Physics::TFront(double L, double f) // аргументы - безразм. величины
{
    double res;
    if (Config::TFrontMode == 1)
    {
        res = -rhoI * gC * (L - f);
    }
    else
        res = 0;

    return res;
}

void Physics::InitAccumCoef(std::string filepath)
{
    accumCoef.resize(3);
    for (int i = 0; i < 3; i++)
    {
        accumCoef[i].resize(3);
    }

    std::ifstream file(filepath);

    for (int j = 2; j > -1; j--)
    {
        for (int i = 0; i < 3; i++)
        {
            file >> accumCoef[i][j];
            accumCoef[i][j] *= (j + 1);
        }
    }

    file.close();
}

double Physics::accumulation(double t) // аргументы - безразм. величины
{
    t = fmod(t / 86400, 365);

    double res = 0;

    if (0 <= t && t < 105)
        res = accumCoef[0][0] + accumCoef[0][1] * (t + 107) + accumCoef[0][2] * (t + 107) * (t + 107);
    else if (105 <= t && t < 166)
        res = accumCoef[1][0] + accumCoef[1][1] * (t - 105) + accumCoef[1][2] * (t - 105) * (t - 105);
    else if (166 <= t && t < 258)
        res = accumCoef[2][0] + accumCoef[2][1] * (t - 166) + accumCoef[2][2] * (t - 166) * (t - 166);
    else
        res = accumCoef[0][0] + accumCoef[0][1] * (t - 258) + accumCoef[0][2] * (t - 258) * (t - 258);

    return (res);
}

double Physics::Surface(double t, double LPrev, double dt, double df)
{
    double res = LPrev;

    if (Config::AccumulationMode == 1)
    {
        res += accumulation(t) * dt / 86400;
    }
    if (Config::IceMovementMode == 1)
    {
        res += -(1 - rhoI / rhoW) * df;
    }

    return res;
}
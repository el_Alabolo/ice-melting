/* Класс моделирования процесса */

#include "FFMuni.h"
#include "Config.h"
#include "Input.h"
#include "Physics.h"

#include <cmath>
#include <fstream>
#include <iostream>

//
using P = Physics;

FFMuni::FFMuni(InputString inputString) // Конструктор задачи для решения методом FFM
{
    xx = inputString.x;
    yy = inputString.y;
    tWrotenPrev = 0;

    // Размерность задачи
    n = std::round(Config::nodesPerMeter * (inputString.surface - inputString.bedrock)) + 1;

    // Координатная сетка для y
    y.resize(n, 0.0);
    mesh();

    // Временная сетка
    tCur = 0;
    tPrev = 0;
    dt = Config::timeStep;

    // Поток и высота дна
    flux = inputString.flux;
    bedrock = inputString.bedrock;

    // Положение поверхности и фронта относительно дна
    LCur = inputString.surface - inputString.bedrock;
    fDoubleCur = LCur - inputString.iceThickness;
    LPrev = 0;
    fDoublePrev = 0;
    df = 0;
    df1 = 0;
    df2 = 0;

    // Определение количества и типов фаз
    if (fDoubleCur <= y[1] * LCur)
    {
        fDoubleCur = 0;
        type = "I";
    }
    else if (fDoubleCur >= y[n - 2] * LCur)
    {
        fDoubleCur = LCur;
        type = "W";
    }
    else
        type = "WI";

    // Подготовка матриц и векторов системы
    TPrevI.resize(n);
    TCurI.resize(n);
    TCurIterI.resize(n);
    MI = Matrix3(n);

    TPrevW.resize(n);
    TCurW.resize(n);
    TCurIterW.resize(n);
    MW = Matrix3(n);

    // Параметры погрешности итераций и их количества
    iterationsLimiter = Config::iterationsLimiter;
    iterationsCounter = 0;
    iterationsAccuracy = Config::iterationsAccuracy;
    errW = 0;
    errI = 0;

    // Заполнение вектора температуры начальными данными следующим образом:
    // фаза воды в т/д равновесии с учетом потока на дне
    // фаза льда интерполируется линейно по положению и температуре в точках фронта и поверхности

    if (type == "W")
    {
        TCurW[n - 1] = P::TSurface(0, LCur + inputString.bedrock);

        for (int i = n - 2; i >= 0; i--)
        {
            TCurW[i] = TCurW[n - 1] + inputString.flux * fDoubleCur / P::lambdaW * dy * (n - 1 - i);
        }
    }

    if (type == "I")
    {
        TCurI[n - 1] = P::TSurface(0, LCur + inputString.bedrock);
        TCurI[0] = P::TFront(LCur, fDoubleCur);

        for (int i = n - 2; i >= 0; i--)
        {
            TCurI[i] = (TCurI[n - 1] - TCurI[0]) * y[i] + TCurI[0];
        }
    }

    if (type == "WI")
    {

        TCurW[n - 1] = P::TFront(LCur, fDoubleCur);

        TCurI[0] = TCurW[n - 1];
        TCurI[n - 1] = P::TSurface(0, LCur + inputString.bedrock);

        for (int i = n - 2; i >= 0; i--)
        {
            TCurW[i] = TCurW[n - 1] + inputString.flux * fDoubleCur / P::lambdaW * dy * (n - 1 - i);
        }
        for (int i = 0; i < n; i++)
        {
            TCurI[i] = (TCurI[n - 1] - TCurI[0]) * y[i] + TCurI[0];
        }
    }

    filename = "../Output/FFMuni" + Config::scheme + "_" + std::to_string(xx) + "," + std::to_string(yy);

    std::ofstream file(filename + "_surf.txt");
    file.clear();
    file << tCur/31536000 << '\t'
         << fDoubleCur << '\t'
         << LCur << '\n';
    file.close();

    file.open(filename + "_T.txt");
    file.clear();

    if (type == "I")
        for (int i = 0; i < n; i++)
            file << y[i] * LCur << '\t'
                 << TCurI[i] << '\n';

    else if (type == "W")
        for (int i = 0; i < n; i++)
            file << y[i] * LCur << '\t'
                 << TCurW[i] << '\n';

    else
    {
        for (int i = 0; i < n; i++)
            file << y[i] * fDoubleCur << '\t'
                 << TCurW[i] << '\n';
        for (int i = 0; i < n; i++)
            file << y[i] * (LCur - fDoubleCur) << '\t'
                 << TCurI[i] << '\n';
    }
    file.close();
}

void FFMuni::step() // Переход задачи на dt и решение методом FFM
{
    // ЗАПИСЬ ПЕРЕМЕННЫХ НА ПРЕДЫДУЩЕМ ВРЕМЕННОМ СЛОЕ //
    LPrev = LCur;                     //
    fDoublePrev = fDoubleCur;         //
                                      //
    tPrev = tCur;                     //
    tCur = tPrev + dt;                //
                                      //
    Maths::CopyVector(TCurI, TPrevI); // Запись тепературы на предыдущем временном слое
    Maths::CopyVector(TCurW, TPrevW); //

    // СЛУЧАЙ ОДНОЙ ФАЗЫ: dt фиксировано, df = 0 //
    if (type != "WI")                           //
    {                                           //
        LCur = P::Surface(tPrev, LPrev, dt, 0); // Вычисление LCur
                                                //
        if (type == "W")                        // Вода
        {                                       //
            fDoubleCur = 1;                     //
            system();                           //
            Maths::Thomas(MW, TCurW);           //
        }                                       //
                                                //
        if (type == "I")                        // Лед
        {                                       //
            fDoubleCur = 0;                     //
            system();                           //
            Maths::Thomas(MI, TCurI);           //
        }                                       //
    }
    // СЛУЧАЙ ДВУХ ФАЗ: dt фиксировано, итерационно ищем df //                                               //
    else                                             //
    {                                                //
        df = 0;                                      // Начальное приближение df
        iterationsCounter = 0;                       //
        do                                           //
        {                                            //
            Maths::CopyVector(TCurI, TCurIterI);     // Копирование итерационной тепературы льда
            Maths::CopyVector(TCurW, TCurIterW);     // Копирование итерационной тепературы воды
                                                     //
            calc_df();                               // Оценка df
                                                     //
            LCur = P::Surface(tPrev, LPrev, dt, df); // Оценки LCur и fDoubleCur
            fDoubleCur = fDoublePrev + df;           //
                                                     //
            system();                                // Система
                                                     //
            Maths::Thomas(MW, TCurW);                // Решение системы
            Maths::Thomas(MI, TCurI);                //
                                                     //
            errW = Maths::Norm(TCurW, TCurIterW);    // Погрешности итераций
            errI = Maths::Norm(TCurI, TCurIterI);    //
                                                     //
            iterationsCounter++;                     //
        } while ((errW > Config::iterationsAccuracy && errI > Config::iterationsAccuracy) && iterationsCounter < iterationsLimiter);
    }

    findFront(); // Проверка на появление/исчезновение фазы ( = поиск пололжения фронта)
                 //

    if (/* tCur - tWrotenPrev >= Config::writingPeriod * dt */ true)                                                          // Вывод результатов
    {                                                                                                                         //
        tWrotenPrev = tCur;                                                                                                   //
        std::ofstream file("../Output/FFMuni " + std::to_string(xx) + ',' + std::to_string(yy) + ".txt", std::ios_base::app); //
        file << tCur / 31536000 << '\t' << fDoubleCur << '\t' << LCur << '\n';                                                //
        file.close();                                                                                                         //

        file.open("../uni.txt");

        for (int i = 0; i < n; i++)
            file << y[i] * fDoubleCur << '\t' << TCurW[i] << '\n';
        for (int i = 0; i < n; i++)
            file << fDoubleCur + y[i] * (LCur - fDoubleCur) << '\t' << TCurI[i] << '\n';

        file.close();
    }
};

void FFMuni::system() // Процедура для заполнения системы в методе FFM
{
    // Очистка системы перед заполнением

    for (int i = 0; i < n; i++)
    {
        MW.diag[i] = 0;
        MW.low[i] = 0;
        MW.up[i] = 0;

        MI.diag[i] = 0;
        MI.low[i] = 0;
        MI.up[i] = 0;

        TCurW[i] = 0;
        TCurI[i] = 0;
    }

    // Заполнение системы

    if (type == "W")
    {
        MW.diag[0] = 2 * P::aW * dt + dy * dy * LCur * LCur;
        MW.up[0] = -2 * P::aW * dt;
        TCurW[0] = TPrevW[0] * dy * dy * LCur * LCur + 2 * P::aW * dt * dy * LCur * flux / P::lambdaW;

        MW.diag[n - 1] = 1;
        TCurW[n - 1] = P::TSurface(tCur, bedrock + LCur);

        for (int i = 1; i < n - 1; i++)
        {
            MW.diag[i] = 4 * P::aW * dt + 2 * dy * dy * LCur * LCur;
            MW.low[i] = -2 * P::aW * dt + dy * LCur * y[i] * (LCur - LPrev);
            MW.up[i] = -2 * P::aW * dt - dy * LCur * y[i] * (LCur - LPrev);

            TCurW[i] = TPrevW[i] * 2 * dy * dy * LCur * LCur;
        }
    }
    if (type == "I")
    {
        MI.diag[0] = 2 * P::aI * dt + dy * dy * LCur * LCur;
        MI.up[0] = -2 * P::aI * dt;
        TCurI[0] = TPrevI[0] * dy * dy * LCur * LCur + 2 * P::aI * dt * dy * LCur * flux / P::lambdaI;

        MI.diag[n - 1] = 1;
        TCurI[n - 1] = P::TSurface(tCur, bedrock + LCur);

        for (int i = 1; i < n - 1; i++)
        {
            MI.diag[i] = 4 * P::aI * dt + 2 * dy * dy * LCur * LCur;
            MI.low[i] = -2 * P::aI * dt + dy * LCur * y[i] * (LCur - LPrev);
            MI.up[i] = -2 * P::aI * dt - dy * LCur * y[i] * (LCur - LPrev);

            TCurI[i] = TPrevI[i] * 2 * dy * dy * LCur * LCur;
        }
    }
    if (type == "WI")
    {
        MW.diag[0] = 2 * P::aW * dt + dy * dy * fDoubleCur * fDoubleCur;
        MW.up[0] = -2 * P::aW * dt;
        TCurW[0] = TPrevW[0] * dy * dy * fDoubleCur * fDoubleCur + 2 * P::aW * dt * dy * fDoubleCur * flux / P::lambdaW;

        MI.diag[0] = 1;
        TCurI[0] = P::TFront(LCur, fDoubleCur);

        for (int i = 1; i < n - 1; i++)
        {
            MW.diag[i] = 4 * P::aW * dt + 2 * dy * dy * fDoubleCur * fDoubleCur;
            MW.low[i] = -2 * P::aW * dt + y[i] * dy * fDoubleCur * df;
            MW.up[i] = -2 * P::aW * dt - y[i] * dy * fDoubleCur * df;

            TCurW[i] = TPrevW[i] * 2 * dy * dy * fDoubleCur * fDoubleCur;

            MI.diag[i] = 4 * P::aI * dt + 2 * dy * dy * (LCur - fDoubleCur) * (LCur - fDoubleCur);
            MI.low[i] = -2 * P::aI * dt + dy * (LCur - fDoubleCur) * (y[i] * (LCur - LPrev) + (1 - y[i]) * df) + Config::IceMovementMode * dy * (LCur - fDoubleCur) * (1 - P::rhoI / P::rhoW) * df;
            MI.up[i] = -2 * P::aI * dt - dy * (LCur - fDoubleCur) * (y[i] * (LCur - LPrev) + (1 - y[i]) * df) - Config::IceMovementMode * dy * (LCur - fDoubleCur) * (1 - P::rhoI / P::rhoW) * df;

            TCurI[i] = TPrevI[i] * 2 * dy * dy * (LCur - fDoubleCur) * (LCur - fDoubleCur);
        }

        MW.diag[n - 1] = 1;
        TCurW[n - 1] = P::TFront(LCur, fDoubleCur);

        MI.diag[n - 1] = 1;
        TCurI[n - 1] = P::TSurface(tCur, bedrock + LCur);
    }
}

void FFMuni::calc_df() // Вычиление df

{
    df1 = -P::lambdaW / fDoubleCur * (2 * P::aW * dt * (TCurIterW[n - 1] - TCurIterW[n - 2]) + dy * dy * fDoubleCur * fDoubleCur * (TCurIterW[n - 1] - TPrevW[n - 1])) /           //
          (2 * P::aW * dt + dy * fDoubleCur * df);                                                                                                                                 //
    df2 = P::lambdaI / (LCur - fDoubleCur) * (2 * P::aI * dt * (TCurIterI[1] - TCurIterI[0]) - dy * dy * (LCur - fDoubleCur) * (LCur - fDoubleCur) * (TCurIterI[0] - TPrevI[0])) / //
          (2 * P::aI * dt - dy * (LCur - fDoubleCur) * df - Config::IceMovementMode * dy * (LCur - fDoubleCur) * (1 - P::rhoI / P::rhoW) * df);                                    //
                                                                                                                                                                                   //
    df = dt * (df1 + df2) /                                                                                                                                                        //
         (dy * P::Q * (P::rhoI + P::rhoW) / 2);                                                                                                                                    //                                                                                                                                                                                 //
}

void FFMuni::findFront() // Поиск положения фронта в методе FFM
{
    int flag = 0;
    int fIntCur = 0;

    std::vector<double> T(n);

    if (type == "WI")
    {
        if (fDoubleCur <= 0)
        {
            fDoubleCur = 0;
            type = "I";
        }
        if (fDoubleCur >= LCur)
        {
            fDoubleCur = LCur;
            type = "W";
        }
    }
    else
        // Пробегаем по узлам соотв. вектора температур
        for (int i = 1; i < n - 2; i++)
        {
            // Если нашли переход фазы
            if ((TCurI[i] - P::TFront(LCur, y[i] * LCur)) * (TCurI[i + 1] - P::TFront(LCur, y[i + 1] * LCur)) <= 0)
            {
                if (type == "W")
                    Maths::CopyVector(TCurW, T);
                if (type == "I")
                    Maths::CopyVector(TCurI, T);

                type = "WI"; // Ставим 2 фазы
                flag = 1;    //

                // Находим узел границы
                if (fabs(TCurI[i] - P::TFront(LCur, y[i] * LCur)) <= fabs(TCurI[i + 1] - P::TFront(LCur, y[i + 1] * LCur)))
                {
                    fDoubleCur = y[i] * LCur;
                    fIntCur = i;
                }
                else
                {
                    fDoubleCur = y[i + 1] * LCur;
                    fIntCur = i + 1;
                }
                T[fIntCur] = P::TFront(LCur, fDoubleCur);

                // Интерполируем линейно на 2 температурных вектора
                TCurW[0] = T[0];
                TCurW[n - 1] = T[fIntCur];
                TCurI[0] = T[fIntCur];
                TCurI[n - 1] = T[n - 1];

                int kk1 = 0;
                int kk2 = fIntCur;

                for (int j = 0; j < n; j++)
                {
                    for (int k = 0; k < fIntCur; k++)
                    {
                        if (y[k] * LCur <= y[j] * fDoubleCur && y[j] * fDoubleCur < y[k + 1] * LCur)
                        {
                            TCurW[j] = T[k] + (T[k + 1] - T[k]) / (y[k + 1] - y[k]) * (y[j] * fDoubleCur / LCur - y[k]);
                        }
                    }
                }

                for (int j = 0; j < n; j++)
                {
                    for (int k = fIntCur; k < n - 1; k++)
                    {
                        if (y[k] * LCur <= fDoubleCur + y[j] * (LCur - fDoubleCur) && fDoubleCur + y[j] * (LCur - fDoubleCur) < y[k + 1] * LCur)
                        {
                            TCurI[j] = T[k] + (T[k + 1] - T[k]) / (y[k + 1] - y[k]) * (fDoubleCur / LCur + y[j] * (1 - fDoubleCur / LCur) - y[k]);
                        }
                    }
                }

                break;
            }
        }

    print();
}

void FFMuni::mesh()
{
    for (int i = 0; i < n; i++)
        y[i] =
            1.0 / (n - 1) * i;
    // sigmoidF(1.0 / (n - 1) * i);

    dy = 1.0 / (n - 1);
}

FFMuni::~FFMuni()
{
}

void FFMuni::print()
{
    /////////////////////////////////////////////
    std::ofstream file(filename + "_surf.txt", std::ios::app);

    file << tCur / 31536000 << '\t'
         << fDoubleCur << '\t'
         << LCur << '\n';
    
    file.close();
    /////////////////////////////////////////////
    file.open(filename + "_T.txt");
    file.clear();

    if (type == "I")
        for (int i = 0; i < n; i++)
            file << y[i] * LCur << '\t'
                 << TCurI[i] << '\n';

    else if (type == "W")
        for (int i = 0; i < n; i++)
            file << y[i] * LCur << '\t'
                 << TCurW[i] << '\n';

    else
    {
        for (int i = 0; i < n; i++)
            file << y[i] * fDoubleCur << '\t'
                 << TCurW[i] << '\n';
        for (int i = 0; i < n; i++)
            file << y[i] * (LCur - fDoubleCur) << '\t'
                 << TCurI[i] << '\n';
    }
    file.close();
}

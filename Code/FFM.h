/* Класс моделирования методом выпрямления фронта */

#ifndef FFM_H
#define FFM_H

#include "Maths.h"
#include "Config.h"
#include "Input.h"

#include <vector>
#include <string>

class FFM
{
public:
    // данные для записи результатов и режимов работы
    int xx; // x-координата точки
    int yy; // y-координата точки
    // int numOfString;      // номер обрабатываемой точки в файле
    std::string filename; // название файла для данных (без суффиксов)
    std::string type;

    // пространственная сетка

    int n;                  // размерность задачи
                            //
    std::vector<double> y;  // вектор узлов
    std::vector<double> dy; // вектор шагов сетки
                            //
    double LCur;            // положение поверхности текущее
    double LPrev;           // ..................... предыдущее
    double dL;              // изменение положения поверхности
                            //
    double fCur;      // положение границы раздела фаз текущее
    double fPrev;     // ............................. предыдущее
    double df;              // изменение положения границы раздела фаз
    double df1;             //
    double df2;             //
                            //
    double bedrock;         // положение дна относительно уровня моря

    // временная сетка

    double tCur;        // значение времени текущее
    double tPrev;       // ................ предыдущее
    double dt;          //  изменение значения времени
                        //
    double tWrotenPrev; // значение времени при предыдущей записи

    // температура и потоки
    std::vector<double> TCurW;     // вектор температур для фазы воды текущий
    std::vector<double> TCurIterW; // ....................................... на предыдущей итерации
    std::vector<double> TPrevW;    // ............................... предыдущий
                                   //
    std::vector<double> TCurI;     // вектор температур для фазы воды текущий
    std::vector<double> TCurIterI; // ....................................... на предыдущей итерации
    std::vector<double> TPrevI;    // ............................... предыдущий
                                   //
    double flux;                   // тепловой поток на дне
                                   //

    // переменные для матриц и систем

    Matrix3 MW;                // матрица для фазы воды
    Matrix3 MI;                // ................ льда
                               //
    int iterationsLimiter;     // ограничитель итераций
    int iterationsCounter;     // счетчик итераций
    double iterationsAccuracy; // требуемая точность после итераций
    double errW;               // отклонение итераций в фазе воды
    double errI;               // .......................... льда

    // функции
    FFM(InputString inputString);                     // конструктор
    ~FFM();                                           // деструктор
    void step();                                      // решение задачи на один шаг
    void system();                                    // заполнение системы
    void calc_df();                                   // вычисление изменения положения фронта
    void mesh();                                      // построение сетки по координате
    static double sigmoidF(double x /* от 0 до 1 */); // сигмоида для
    double D(int i);                                  //
    void print();                                     // запись результата в файл
    void printDescription();                          // запись описания в файл
    void checkFront();
};

#endif /* FFM_H */
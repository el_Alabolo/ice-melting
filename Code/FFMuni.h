/* Класс моделирования методом выпрямления фронта */

#ifndef FFMUNI_H
#define FFMUNI_H

#include "Maths.h"
#include "Config.h"
#include "Input.h"

#include <vector>
#include <string>

class FFMuni
{
private:
    int xx;
    int yy;

    int numOfString;

    int n;

    std::vector<double> y;
    double dy;

public:
    double LPrev;
    double LCur;

private:
    double fDoubleCur;
    double fDoublePrev;

    double df;
    double df1;
    double df2;

    double flux;
    double bedrock;

    double tWrotenPrev;

public:
    double tCur;

private:
    double tPrev;

    double dt;

    std::vector<double> TPrevW;
    std::vector<double> TCurW;
    std::vector<double> TCurIterW;

    std::vector<double> TPrevI;
    std::vector<double> TCurI;
    std::vector<double> TCurIterI;

    Matrix3 MI;
    Matrix3 MW;

    int iterationsLimiter;
    int iterationsCounter;
    double iterationsAccuracy;
    double errW;
    double errI;

    std::string type;

    std::string filename;

public:
    FFMuni(InputString inputString);
    ~FFMuni();
    void step();
    void findFront();
    void system();
    void calc_df();
    void mesh();
    void print();
};

#endif /* FFMUNI_H */
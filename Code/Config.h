/* Класс конфигурации программы */

#ifndef CONFIG_H
#define CONFIG_H

#include <string>

class Config
{
private:
    static const std::string ConfigPath; // путь к конфигурационному файлу
public:
    static int nodesPerMeter;
    static double timeStep;
    static int writingPeriod;
    static double finalTime;
    static bool TSurfaceMode;
    static double TSurface;
    static bool TFrontMode;
    static bool AccumulationMode;
    static bool IceMovementMode;
    static bool VTSMode;
    static bool FFMMode;
    static double iterationsAccuracy;
    static int iterationsLimiter;
    static std::string scheme;
    static std::string mesh;

    static void initConfig();
};

#endif /* CONFIG_H */
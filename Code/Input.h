/* Класс входных данных */

#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <string>

struct InputString
{
    int x;
    int y;
    double bedrock;
    double surface;
    double iceThickness;
    double flux;
};

class Input
{
public:
    std::vector<InputString> Data;
    int numOfStrings;

    Input(std::string filepath);
};

#endif /* INPUT_H */
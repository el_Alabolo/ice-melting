/* Класс моделирования методом ловли фронта */

#ifndef VTSUNI_H
#define VTSUNI_H

#include "Maths.h"
#include "Config.h"
#include "Input.h"

#include <vector>
#include <string>

class VTSuni
{
private:
    int n;

    std::vector<double> y;
    double dy;

    double LPrev;
    double LCur;
    double dL;

    int fIntCur;
    int fIntPrev;
    double fDoubleCur;
    double fDoublePrev;

    double flux;
    double bedrock;
public:
    double tCur;
private:    
    double tPrev;
    
    double dt;

    double dtVTS;
    double dtVTSup;
    double dtVTSlow1;
    double dtVTSlow2;
    double dtVTSlow3;

    std::vector<double> TPrev;
    std::vector<double> TCur;
    std::vector<double> TCurIter;

    Matrix3 M;

    int iterationsLimiter;
    int iterationsCounter;
    double iterationsAccuracy;
    double err;

    std::string type;

    double tWrotenPrev;
    int xx;
    int yy;

    std::string filename;

public:
    VTSuni(InputString inputString);
    ~VTSuni();

    void findFront();
    void step();
    void system();
    void systemCN();
    void calc_dtVTS();
    void print();
};


#endif /* VTSUNI_H */
/* Класс процедуры обезразмеривания */

#ifndef NONDIM_H
#define NONDIM_H

class NonDim
{
public:
    static double x0;
    static double t0;
    static double a0;
    static double lambda0;
    static double rho0;
    static double Q0;
    static double q0;
    static double T0;
    static double TGrad0;
    static double gC0;
};

#endif /* NONDIM_H */
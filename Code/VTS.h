/* Класс моделирования методом ловли фронта */

#ifndef VTS_H
#define VTS_H

#include "Maths.h"
#include "Config.h"
#include "Input.h"

#include <vector>
#include <string>

class VTS
{
public:
    int n;

    std::vector<double> y;
    std::vector<double> dy;
    double dyVTS;

    double LPrev;
    double LCur;
    double dL;

    int fIntCur;
    int fIntPrev;
    double fDoubleCur;
    double fDoublePrev;

    double flux;
    double bedrock;
public:
    double tCur;    
    double tPrev;
    
    double dt;

    double dtVTS;
    double dtVTSup;
    double dtVTSlow1;
    double dtVTSlow2;
    double dtVTSlow3;

    std::vector<double> TPrev;
    std::vector<double> TCur;
    std::vector<double> TCurIter;

    Matrix3 M;

    int iterationsLimiter;
    int iterationsCounter;
    double iterationsAccuracy;
    double err;

    std::string type;

    double tWrotenPrev;
    int xx;
    int yy;

    std::string filename;

public:
    VTS(InputString inputString);
    ~VTS();

    void findFront();
    void step();
    void system();
    void calc_dtVTS();
    void print();

    void mesh();
    static double sigmoidF(double x);
    double D(int i);
    void initGuess();

    void printDescription();

};


#endif /* VTS_H */
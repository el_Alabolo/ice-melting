/* Класс конфигурации программы*/

#include "Config.h"

#include <string>
#include <regex>
#include <sstream>
#include <fstream>

/* Инициализация */

const std::string Config::ConfigPath = "../Config.txt"; // путь к конфигурационному файлу

int Config::nodesPerMeter = 0;
double Config::timeStep = 0;
int Config::writingPeriod = 0;
double Config::finalTime = 0;
bool Config::TSurfaceMode = false;
double Config::TSurface = 0.0;
bool Config::TFrontMode = false;
bool Config::AccumulationMode = false;
bool Config::IceMovementMode = false;
bool Config::VTSMode = false;
bool Config::FFMMode = false;
double Config::iterationsAccuracy = 0;
int Config::iterationsLimiter = 0;
std::string Config::scheme = "implicit";
std::string Config::mesh = "uni";

void Config::initConfig()
{
    std::ifstream file(ConfigPath);

    std::stringstream stream;

    std::string line;
    std::cmatch lineParts;

    std::regex regularExpr("([A-Za-z]{1,})"
                           "([=]{1,1})"
                           "([A-Za-z-+0-9.]{1,})");
    int valueInt;
    double valueDouble;

    while (!file.eof())
    {
        std::getline(file, line);
        if (std::regex_match(line.c_str(), lineParts, regularExpr))
        {
            stream.str("");
            stream.clear();

            stream << lineParts[3];

            if (lineParts[1] == "nodesPerMeter")
            {
                stream >> nodesPerMeter;
                continue;
            }
            if (lineParts[1] == "timeStep")
            {
                stream >> timeStep;
                timeStep *= 3600;
                continue;
            }
            if (lineParts[1] == "writingPeriod")
            {
                stream >> writingPeriod;
                continue;
            }
            if (lineParts[1] == "finalTime")
            {
                stream >> finalTime;
                finalTime *= 31536000;
                continue;
            }
            if (lineParts[1] == "TSurfaceMode")
            {
                stream >> TSurfaceMode;
                continue;
            }
            if (lineParts[1] == "TSurface")
            {
                stream >> TSurface;
                continue;
            }
            if (lineParts[1] == "TFrontMode")
            {
                stream >> TFrontMode;
                continue;
            }
            if (lineParts[1] == "AccumulationMode")
            {
                stream >> AccumulationMode;
                continue;
            }
            if (lineParts[1] == "IceMovementMode")
            {
                stream >> IceMovementMode;
                continue;
            }
            if (lineParts[1] == "VTSMode")
            {
                stream >> VTSMode;
                continue;
            }
            if (lineParts[1] == "FFMMode")
            {
                stream >> FFMMode;
                continue;
            }
            if (lineParts[1] == "iterationsAccuracy")
            {
                stream >> iterationsAccuracy;
                continue;
            }
            if (lineParts[1] == "iterationsLimiter")
            {
                stream >> iterationsLimiter;
                continue;
            }
            if (lineParts[1] == "scheme")
            {
                stream >> scheme;
                continue;
            }
            if (lineParts[1] == "mesh")
            {
                stream >> mesh;
                continue;
            }

            stream.str("");
            stream.clear();
        }
    }
    file.close();
}

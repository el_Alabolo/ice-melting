/* Класс моделирования методом ловли фронта */

#include "VTSuni.h"
#include "Config.h"
#include "Input.h"
#include "Physics.h"

#include <cmath>
#include <fstream>
#include <iostream>

using P = Physics;

VTSuni::VTSuni(InputString inputString) // Конструктор задачи для решения методом VTS
{

    xx = inputString.x;
    yy = inputString.y;
    tWrotenPrev = 0;

    // Размерность задачи
    n = std::round(Config::nodesPerMeter * (inputString.surface - inputString.bedrock)) + 1;

    // Координатная сетка для y
    y.resize(n);
    dy = 1.0 / (n - 1);
    for (int i = 0; i < n; i++)
        y[i] = i * dy;

    // Временная сетка
    tCur = 0;
    dt = Config::timeStep;
    dtVTS = dt;

    // Поток и высота дна
    flux = inputString.flux;
    bedrock = inputString.bedrock;

    // Положение поверхности и фронта относительно дна
    LCur = inputString.surface - inputString.bedrock;
    fIntCur = round((LCur - inputString.iceThickness) / (dy * LCur));
    fDoubleCur = y[fIntCur] * LCur;

    // Определение количества и типов фаз
    if (fIntCur == 0)
        type = "I";
    else if (fIntCur == n - 1)
        type = "W";
    else
        type = "WI";

    // Подготовка матрицы системы и векторов
    TPrev.resize(n);
    TCur.resize(n);
    TCurIter.resize(n);
    M = Matrix3(n);

    // Параметры погрешности итераций и их количества
    iterationsLimiter = Config::iterationsLimiter;
    iterationsCounter = 0;
    iterationsAccuracy = Config::iterationsAccuracy;
    err = 0;

    // Заполнение вектора температуры начальными данными следующим образом:
    // фаза воды в т/д равновесии с учетом потока на дне
    // фаза льда интерполируется линейно по положению и температуре в точках фронта и поверхности

    TCur[n - 1] = P::TSurface(0, LCur + inputString.bedrock);

    if (type == "W")
    {
        for (int i = n - 2; i >= 0; i--)
        {
            TCur[i] = TCur[fIntCur] + inputString.flux * LCur / P::lambdaW * dy * (n - 1 - i);
        }
    }

    if (type == "I")
    {
        TCur[0] = P::TFront(LCur, fDoubleCur);

        for (int i = n - 2; i >= 0; i--)
        {
            TCur[i] = (TCur[n - 1] - TCur[0]) * y[i] + TCur[fIntCur];
        }
    }

    if (type == "WI")
    {

        TCur[fIntCur] = P::TFront(LCur, fDoubleCur);

        for (int i = fIntCur - 1; i >= 0; i--)
        {
            TCur[i] = TCur[fIntCur] + inputString.flux * LCur / P::lambdaW * dy * (fIntCur - i);
        }
        for (int i = fIntCur + 1; i < n; i++)
        {
            TCur[i] = (TCur[n - 1] - TCur[fIntCur]) * ((y[i] - y[fIntCur]) / (y[n - 1] - y[fIntCur])) + TCur[fIntCur];
        }
    }

    filename = "../Output/VTS_" + std::to_string(xx) + "," + std::to_string(yy);

    std::ofstream file(filename + "_surf.txt");
    file.clear();

    file
        << tCur/31536000 << '\t'
        << fDoubleCur << '\t'
        << LCur << '\n';
    file.close();

    file.open(filename + "_T.txt");
    file.clear();
    for (int i = 0; i < n; i++)
        file << y[i] * LCur << '\t'
             << TCur[i] << '\n';

    file.close();
}

void VTSuni::step() // Переход задачи на dt и решение методом VTS
{
    // ЗАПИСЬ ПЕРЕМЕННЫХ НА ПРЕДЫДУЩЕМ ВРЕМЕННОМ СЛОЕ //
    LPrev = LCur;                   //
                                    //
    fIntPrev = fIntCur;             //
    fDoublePrev = fDoubleCur;       //
                                    //
    tPrev = tCur;                   //
                                    //
    Maths::CopyVector(TCur, TPrev); //

    // СЛУЧАЙ ОДНОЙ ФАЗЫ: dt фиксировано, df = 0 //
    if (type != "WI")                           //
    {                                           //
        tCur = tPrev + dt;                      // Вычисление tCur и LCur
        LCur = P::Surface(tPrev, LPrev, dt, 0); // так как dt фиксировано
                                                //
        if (type == "W")                        // Вода
        {                                       //
            fIntCur = n - 1;                    //
            fDoubleCur = LCur;                  //
                                                //
            systemCN();                         //
        }                                       //
        if (type == "I")                        // Лед
        {                                       //
            fIntCur = 0;                        //
            fDoubleCur = 0;                     //
                                                //
            systemCN();                         //
        }                                       //
        Maths::Thomas(M, TCur);                 //
    }                                           //

    // СЛУЧАЙ ДВУХ ФАЗ: df=+-dy, итерационно ищем dt //
    else                                                                     //
    {                                                                        //
        iterationsCounter = 0;                                               //
        dtVTS = dt * Maths::sign(dtVTS);                                     //
        do                                                                   //
        {                                                                    //
            Maths::CopyVector(TCur, TCurIter);                               //
                                                                             //
            calc_dtVTS();                                                    //
                                                                             //
            fIntCur = fIntPrev + Maths::sign(dtVTS);                         //
            fDoubleCur = y[fIntCur] * LCur;                                  //
                                                                             //
            tCur = tPrev + fabs(dtVTS);                                      //
                                                                             //
            LCur = P::Surface(tPrev, LPrev, dtVTS, dy * Maths::sign(dtVTS)); //
                                                                             //
            systemCN();                                                      //
                                                                             //
            Maths::Thomas(M, TCur);                                          //
                                                                             //
            err = Maths::Norm(TCur, TCurIter);                               //
            iterationsCounter++;
            if (iterationsCounter >= iterationsLimiter)
                std::cout << "+" + '\n';                                             //
                                                                                     //
        } while (err > iterationsAccuracy && iterationsCounter < iterationsLimiter); //
    }                                                                                //

    findFront();

    print();

    /* write */
    /* save vars */
}

void VTSuni::system() // Процедура для заполнения системы в методе VTS
{
    // Очистка системы перед заполнением

    for (int i = 0; i < n; i++)
    {
        M.diag[i] = 0;
        M.low[i] = 0;
        M.up[i] = 0;

        TCur[i] = 0;
    }

    // Заполнение системы

    if (type == "W")
    {
        M.diag[0] = 2 * P::aW * dt + dy * dy * LCur * LCur;
        M.up[0] = -2 * P::aW * dt;
        TCur[0] = TPrev[0] * dy * dy * LCur * LCur + 2 * P::aW * dt * dy * LCur * flux / P::lambdaW;

        M.diag[n - 1] = 1;
        TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

        for (int i = 1; i < n - 1; i++)
        {
            M.diag[i] = 4 * P::aW * dt + 2 * dy * dy * LCur * LCur;
            M.low[i] = -2 * P::aW * dt + dy * LCur * y[i] * (LCur - LPrev);
            M.up[i] = -2 * P::aW * dt - dy * LCur * y[i] * (LCur - LPrev);

            TCur[i] = TPrev[i] * 2 * dy * dy * LCur * LCur;
        }
    }
    if (type == "I")
    {
        M.diag[0] = 2 * P::aI * dt + dy * dy * LCur * LCur;
        M.up[0] = -2 * P::aI * dt;
        TCur[0] = TPrev[0] * dy * dy * LCur * LCur + 2 * P::aI * dt * dy * LCur * flux / P::lambdaI;

        M.diag[n - 1] = 1;
        TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

        for (int i = 1; i < n - 1; i++)
        {
            M.diag[i] = 4 * P::aI * dt + 2 * dy * dy * LCur * LCur;
            M.low[i] = -2 * P::aI * dt + dy * LCur * y[i] * (LCur - LPrev);
            M.up[i] = -2 * P::aI * dt - dy * LCur * y[i] * (LCur - LPrev);

            TCur[i] = TPrev[i] * 2 * dy * dy * LCur * LCur;
        }
    }
    if (type == "WI")
    {
        M.diag[0] = 2 * P::aW * fabs(dtVTS) + dy * dy * LCur * LCur;
        M.up[0] = -2 * P::aW * fabs(dtVTS);

        TCur[0] = TPrev[0] * dy * dy * LCur * LCur + 2 * P::aW * fabs(dtVTS) * dy * LCur * flux / P::lambdaW;

        for (int i = 1; i < fIntCur; i++)
        {
            M.diag[i] = 4 * P::aW * fabs(dtVTS) + 2 * dy * dy * LCur * LCur;
            M.low[i] = -2 * P::aW * fabs(dtVTS) + y[i] * dy * LCur * (LCur - LPrev);
            M.up[i] = -2 * P::aW * fabs(dtVTS) - y[i] * dy * LCur * (LCur - LPrev);

            TCur[i] = TPrev[i] * 2 * dy * dy * LCur * LCur;
        }

        M.diag[fIntCur] = 1;
        TCur[fIntCur] = P::TFront(LCur, fDoubleCur);

        for (int i = fIntCur + 1; i < n - 1; i++)
        {
            M.diag[i] = 4 * P::aI * fabs(dtVTS) + 2 * dy * dy * LCur * LCur;
            M.low[i] = -2 * P::aI * fabs(dtVTS) + y[i] * dy * LCur * (LCur - LPrev) + Config::IceMovementMode * dy * LCur * (1 - P::rhoI / P::rhoW) * (dy / Maths::sign(dtVTS) * LCur + fDoubleCur * (LCur - LPrev));
            M.up[i] = -2 * P::aI * fabs(dtVTS) - y[i] * dy * LCur * (LCur - LPrev) - Config::IceMovementMode * dy * LCur * (1 - P::rhoI / P::rhoW) * (dy / Maths::sign(dtVTS) * LCur + fDoubleCur * (LCur - LPrev));

            TCur[i] = TPrev[i] * 2 * dy * dy * LCur * LCur;
        }

        M.diag[n - 1] = 1;
        TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);
    }
}

void VTSuni::systemCN()
{
    // Очистка системы перед заполнением

    for (int i = 0; i < n; i++)
    {
        M.diag[i] = 0;
        M.low[i] = 0;
        M.up[i] = 0;

        TCur[i] = 0;
    }

    // Заполнение системы

    if (type == "W")
    {
        M.diag[0] = 2 * P::aW * dt + dy * dy * LCur * LCur;
        M.up[0] = -2 * P::aW * dt;
        TCur[0] = TPrev[0] * dy * dy * LCur * LCur + 2 * P::aW * dt * dy * LCur * flux / P::lambdaW;

        M.diag[n - 1] = 1;
        TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

        for (int i = 1; i < n - 1; i++)
        {
            double value = 0.25 * dy * y[i] * (LCur * LCur - LPrev * LPrev) - Physics::aW * fabs(dtVTS);

            M.low[i] = value - Physics::aW * fabs(dtVTS);
            M.diag[i] = dy * dy * (LCur * LCur + LPrev * LPrev) + 2 * Physics::aW * fabs(dtVTS);
            M.up[i] = -value - Physics::aW * fabs(dtVTS);

            TCur[i] = TPrev[i - 1] * (-M.low[i]) +
                      TPrev[i] * (dy * dy * (LCur * LCur + LPrev * LPrev) - 2 * Physics::aW * fabs(dtVTS)) +
                      TPrev[i + 1] * (-M.up[i]);
        }
    }
    if (type == "I")
    {
        M.diag[0] = 2 * P::aI * dt + dy * dy * LCur * LCur;
        M.up[0] = -2 * P::aI * dt;
        TCur[0] = TPrev[0] * dy * dy * LCur * LCur + 2 * P::aI * dt * dy * LCur * flux / P::lambdaI;

        M.diag[n - 1] = 1;
        TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);

        for (int i = 1; i < n - 1; i++)
        {
            double value = 0.25 * dy * y[i] * (LCur * LCur - LPrev * LPrev) - Physics::aI * fabs(dtVTS);

            M.low[i] = value - Physics::aI * fabs(dtVTS);
            M.diag[i] = dy * dy * (LCur * LCur + LPrev * LPrev) + 2 * Physics::aI * fabs(dtVTS);
            M.up[i] = -value - Physics::aI * fabs(dtVTS);

            TCur[i] = TPrev[i - 1] * (-M.low[i]) +
                      TPrev[i] * (dy * dy * (LCur * LCur + LPrev * LPrev) - 2 * Physics::aI * fabs(dtVTS)) +
                      TPrev[i + 1] * (-M.up[i]);
        }
    }
    if (type == "WI")
    {
        M.diag[0] = 2 * P::aW * fabs(dtVTS) + dy * dy * LCur * LCur;
        M.up[0] = -2 * P::aW * fabs(dtVTS);

        TCur[0] = TPrev[0] * dy * dy * LCur * LCur + 2 * P::aW * fabs(dtVTS) * dy * LCur * flux / P::lambdaW;

        for (int i = 1; i < fIntCur; i++)
        {
            double value = 0.25 * dy * y[i] * (LCur * LCur - LPrev * LPrev) - Physics::aW * fabs(dtVTS);

            M.low[i] = value - Physics::aW * fabs(dtVTS);
            M.diag[i] = dy * dy * (LCur * LCur + LPrev * LPrev) + 2 * Physics::aW * fabs(dtVTS);
            M.up[i] = -value - Physics::aW * fabs(dtVTS);

            TCur[i] = TPrev[i - 1] * (-M.low[i]) +
                      TPrev[i] * (dy * dy * (LCur * LCur + LPrev * LPrev) - 2 * Physics::aW * fabs(dtVTS)) +
                      TPrev[i + 1] * (-M.up[i]);
        }

        M.diag[fIntCur] = 1;
        TCur[fIntCur] = P::TFront(LCur, fDoubleCur);

        for (int i = fIntCur + 1; i < n - 1; i++)
        {
            double value = 0.25 * dy * (y[i] * (LCur * LCur - LPrev * LPrev) + (1 - Physics::aI / Physics::aW) * (dy / Maths::sign(dtVTS) * 0.5 * (LCur + LPrev) * (LCur + LPrev) + (LCur * LCur - LPrev * LPrev) * 0.5 * (fDoubleCur + fDoublePrev)));

            M.low[i] = value - Physics::aI * fabs(dtVTS);
            M.diag[i] = dy * dy * (LCur * LCur + LPrev * LPrev) + 2 * Physics::aI * fabs(dtVTS);
            M.up[i] = -value - Physics::aI * fabs(dtVTS);

            TCur[i] = TPrev[i - 1] * (-M.low[i]) +
                      TPrev[i] * (dy * dy * (LCur * LCur + LPrev * LPrev) - 2 * Physics::aI * fabs(dtVTS)) +
                      TPrev[i + 1] * (-M.up[i]);
        }

        M.diag[n - 1] = 1;
        TCur[n - 1] = P::TSurface(tCur, bedrock + LCur);
    }
    system();
}

void VTSuni::calc_dtVTS() // Вычисление dtVTS

{
    if (fIntCur == 0) // Если фронт смещается в узел 0
    {
        dtVTSup = dy * dy * LCur * LCur * P::Q * (P::rhoW + P::rhoI) / 2;
        dtVTSlow1 = dy * LCur * flux;
        dtVTSlow2 = P::lambdaI * (2 * P::aI * fabs(dtVTS) * (TCurIter[1] - TCurIter[0]) - dy * dy * LCur * LCur * (TCurIter[0] - TPrev[0])) /
                    (2 * P::aI * fabs(dtVTS) - Config::IceMovementMode * dy * LCur * (1 - P::rhoI / P::rhoW) * dy * LCur / Maths::sign(dtVTS));
    }
    if (fIntCur == n - 1) // Если фронт смещается в узел n-1
    {
        dtVTSup = dy * LCur * P::Q * (P::rhoW + P::rhoI) / 2 * (dy * LCur + (LCur - LPrev) / Maths::sign(dtVTS));
        dtVTSlow1 = -P::lambdaW * (2 * P::aW * fabs(dtVTS) * (TCurIter[n - 1] - TCurIter[n - 2]) + dy * dy * LCur * LCur * (TCurIter[n - 1] - TPrev[n - 1])) /
                    (2 * P::aW * fabs(dtVTS) + dy * LCur * (LCur - LPrev));
        dtVTSlow2 = 0;
    }
    if (fIntCur != 0 && fIntCur != n - 1) // Если фронт смещается в неграничный узел
    {
        dtVTSup = dy * LCur * P::Q * (P::rhoI + P::rhoW) / 2 * (dy * LCur + (LCur - LPrev) / Maths::sign(dtVTS) * y[fIntCur]);
        dtVTSlow1 = -P::lambdaW * (2 * P::aW * fabs(dtVTS) * (TCurIter[fIntCur] - TCurIter[fIntCur - 1]) + dy * dy * LCur * LCur * (TCurIter[fIntCur] - TPrev[fIntCur])) /
                    (2 * P::aW * fabs(dtVTS) + y[fIntCur] * dy * LCur * (LCur - LPrev));
        dtVTSlow2 = P::lambdaI * (2 * P::aI * fabs(dtVTS) * (TCurIter[fIntCur + 1] - TCurIter[fIntCur]) - dy * dy * LCur * LCur * (TCurIter[fIntCur] - TPrev[fIntCur])) /
                    (2 * P::aI * fabs(dtVTS) - y[fIntCur] * dy * LCur * (LCur - LPrev) - Config::IceMovementMode * dy * LCur * (1 - P::rhoI / P::rhoW) * (dy * LCur / Maths::sign(dtVTS) + (LCur - LPrev) * y[fIntCur]));
    }

    dtVTS = dtVTSup / (dtVTSlow1 + dtVTSlow2);
}

void VTSuni::findFront() // Поиск положения фронта в методе VTS

{                                                                                                                     //
    bool flag = 0;                                                                                                    //
    for (int i = 0; i < n - 1; i++)                                                                                   //
        if ((TCur[i] - P::TFront(LCur, y[i] * LCur)) * (TCur[i + 1] - P::TFront(LCur, y[i + 1] * LCur)) <= 0)         //
        {                                                                                                             //
            if (fabs(TCur[i] - P::TFront(LCur, y[i] * LCur)) <= fabs(TCur[i + 1] - P::TFront(LCur, y[i + 1] * LCur))) //
            {                                                                                                         //
                fIntCur = i;                                                                                          //
                fDoubleCur = y[i] * LCur;                                                                             //
                TCur[i] = P::TFront(LCur, fDoubleCur);                                                                //
            }                                                                                                         //
            else                                                                                                      //
            {                                                                                                         //
                fIntCur = i + 1;                                                                                      //
                fDoubleCur = y[i + 1] * LCur;                                                                         //
                TCur[i + 1] = P::TFront(LCur, fDoubleCur);                                                            //
            }                                                                                                         //
            flag = 1;                                                                                                 //
            type = "WI";                                                                                              //
            break;                                                                                                    //
        }                                                                                                             //
    if (flag == 0)                                                                                                    //
    {                                                                                                                 //
        if (TCur.back() < 0)                                                                                          //
            type = "I";                                                                                               //
        else                                                                                                          //
            type = "W";                                                                                               //
    }                                                                                                                 //
}

VTSuni::~VTSuni()
{
}

void VTSuni::print()
{
    std::ofstream file(filename + "_surf.txt", std::ios::app);
    file << tCur << '\t'
         << fDoubleCur << '\t'
         << LCur << '\n';
    file.close();

    file.open(filename + "_T.txt");
    file.clear();
    for (int i = 0; i < n; i++)
        file << y[i] * LCur << '\t'
             << TCur[i] << '\n';
    file.close();
}

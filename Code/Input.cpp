/* Класс входных даных */

#include "Input.h"
#include "Config.h"
#include "cmath"

#include <string>
#include <fstream>
#include <sstream>

Input::Input(std::string filepath)
{
    std::ifstream file(filepath);

    std::string line;
    std::stringstream stream;

    Data.resize(0);

    numOfStrings = 0;

    while (true)
    {
        stream.str("");
        stream.clear();

        std::getline(file, line);

        if (file.eof())
            break;

        Data.resize(Data.size() + 1);

        stream << line;
        stream >>
            Data.back().x >>
            Data.back().y >>
            Data.back().bedrock >>
            Data.back().surface >>
            Data.back().iceThickness >>
            Data.back().flux;
        Data.back().flux /= 1000;
        numOfStrings++;
    }

    stream.str("");
    stream.clear();

    file.close();
}

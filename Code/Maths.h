/* Математические объекты и работа с ними */

#ifndef MATHS_H
#define MATHS_H

#include <vector>

struct Matrix3 // трехдиагональная квадратная матрица
{
    std::vector<double> low;  // субдиагональ
    std::vector<double> diag; // диагональ
    std::vector<double> up;   // наддиагональ

    Matrix3(int n);

    Matrix3();
    ~Matrix3();
};

class Maths
{
private:
public:
    static void CopyVector(std::vector<double> v1, std::vector<double> &v2);
    static double Norm(std::vector<double> v1, std::vector<double> v2);
    static void Thomas(Matrix3 M, std::vector<double> &b); // метод прогонки для решения трехдиагональной системы
    static int sign(double x);
};

#endif /* MATHS_H */
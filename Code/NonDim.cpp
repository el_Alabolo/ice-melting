/* Класс процедуры обезразмеривания */

#include "NonDim.h"

double NonDim::x0 = 1e1;
double NonDim::t0 = 1e9;
double NonDim::a0 = 1e-7;
double NonDim::lambda0 = 1e2;
double NonDim::rho0 = 1e2;
double NonDim::Q0 = 1e5;
double NonDim::q0 = 1e-1;
double NonDim::T0 = 1e-2;
double NonDim::TGrad0 = 1e-3;
double NonDim::gC0 = 1e-5;
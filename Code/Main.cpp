#include "Config.h"
#include "Input.h"
#include "VTSuni.h"
#include "FFM.h"
#include "Physics.h"

#include <iostream>
#include <string>
#include <fstream>
#include <ctime>

int main()
{
    Config::initConfig();
    Input data("../Input/Test.txt");
    Physics::InitAccumCoef("../AccumulationCoefficients.txt");

    //#pragma omp parallel for shared(data)
    for (int i = 0; i < data.Data.size(); i++)
    {

        VTSuni vts(data.Data[i]);
        FFM ffm(data.Data[i]);

        /* while (vts.tCur < Config::finalTime)
        {
            vts.step();
        } */

          while (ffm.tCur < Config::finalTime)
         {
             ffm.step();
         } 
    }
    return 0;
}